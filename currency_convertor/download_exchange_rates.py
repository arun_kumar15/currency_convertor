"""
This method gets the past 90 days exchange rates from the specified url for currency conversion.
It will be called every time the functions starts as the method is called from settings.py and the variable EXCHANGE_RATE_DATA is defined as a global variable.
"""

def get_xml_data():
	import requests
	import xmltodict
	import json

	url = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml'
	response = requests.get(url)
	
	jsonString = json.loads(json.dumps(xmltodict.parse(response.content), indent=4))

	exchange_rates = jsonString['gesmes:Envelope']['Cube']['Cube']

	return exchange_rates
	