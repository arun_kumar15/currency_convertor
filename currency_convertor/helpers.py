import requests
import xmltodict
import json
from currency_convertor.settings import EXCHANGE_RATE_DATA


fetch_convertion_data = EXCHANGE_RATE_DATA

class ConvertCurrency():
	"""
	This class gets the the conversion data and convert the amount from defined src_currency to dest_currency and return the converted amount with destination currency
	"""
	def __init__(self, amount, src_currency_code, dest_currency_code, reference_date):
		self.amount = amount
		self.src_currency = src_currency_code
		self.dest_currency = dest_currency_code
		self.reference_date = reference_date

	def get_exchange_rates_and_convert_amount(self):

		src_rate = None
		dest_rate = None
		converted_amount = None

		try:
			# find the src_rate and dest_rate of src_currency_code and dest_currency code based on the reference date, if exists in the exchange_data
			for datewise_exchange_rates in fetch_convertion_data:
				if datewise_exchange_rates['@time'] == self.reference_date:
					for currency_code_rate_data in datewise_exchange_rates['Cube']:
						if currency_code_rate_data['@currency'] == self.src_currency:
							src_rate = currency_code_rate_data['@rate']
						if currency_code_rate_data['@currency'] == self.dest_currency:
							dest_rate = currency_code_rate_data['@rate']

							break
			if src_rate is not None and dest_rate is not None:
				# calculating the converted_amount
				converted_amount = (float(self.amount) / float(src_rate)) * float(dest_rate)
				# formating the converted amount to 2 decimal places
				converted_amount = format(converted_amount, '.2f')

			return converted_amount

		except Exception as e:
			print(e)
