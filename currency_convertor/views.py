from rest_framework.views import APIView
from django.http import JsonResponse
from django.http import HttpResponse
import requests
from rest_framework.response import Response
from rest_framework import status

from currency_convertor.helpers import ConvertCurrency


class CurrencyConvertorAPI(APIView):

    def get(self, request):
        """
        Convert the requested amount from source currency code to desired currency code based on the conversion value on the reference date
        """

        converted_amount = None

        try:
            amount = self.request.GET.get('amount') or None
            src_currency_code = self.request.GET.get('src_currency') or None
            dest_currency_code = self.request.GET.get('dest_currency') or None
            conversion_reference_date = self.request.GET.get('reference_date') or None

            # is any of the required input is not present, raise bad request error
            if amount is None or src_currency_code is None or dest_currency_code is None or conversion_reference_date is None:
                return Response(
                    {
                        'success': False,
                        'message': 'Incomplete Data, sorry could not convert the requested amount.'
                    },
                    status=status.HTTP_400_BAD_REQUEST)

            # convert the amount
            res = ConvertCurrency(amount, src_currency_code, dest_currency_code, conversion_reference_date)
            converted_amount = res.get_exchange_rates_and_convert_amount()

            # if amount converted, return converted_amount and dest_currency_code with success message
            if converted_amount is not None:
                return Response(
                    {
                        'amount': converted_amount,
                        'currency': dest_currency_code
                    },
                    status=status.HTTP_200_OK)
            else:
                return Response(
                    {
                        'success': False,
                        'message':'Exchange Rates not available for src_currency_code or dest_currency_code or reference_date'
                    },
                    status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            return Response(
                {
                    'success': False,
                    'message':'Sorry could not fetch'
                },
                status=status.HTTP_400_BAD_REQUEST)